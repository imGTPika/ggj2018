﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class RestartGame : NetworkBehaviour
{

    GameObject objectToDestroy;
    GameObject[] objectsToMove;
    GameObject cameraObject;
    public float resetX;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

    }

    //[ClientRpc]
    public void ResetGame() {
        Vector3 oldPosition = transform.position;
        transform.position = new Vector3(resetX, oldPosition.y, oldPosition.z);
    }
}
