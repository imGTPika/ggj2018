﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour {

    public int phase = 0;

    // Skip Tutorial
    private bool skipTriggered = false;
    private int skipClick = 0;

    // Initial Values
    private Vector3 position1, position2;
    private Quaternion rotation1, rotation2;
    private Vector3 velocity1, velocity2;

    // Overall variables
    private bool isRunning = false;
    private float timer = 0f;
    private float startpoint, distance = 0f;
    private GameObject currentBeam;
    public GameObject demo, description;

    // Phase 0: Be familiar with Inputs
    private bool[] p1Control = { false, false, false, false };
    private bool[] p2Control = { false, false, false, false };
    public GameObject beam0, player1, player2;
    public GameObject wasdIcon, arrowIcon, playerTag1, playerTag2;
    
    // Phase 1: Obstacles Demo
    public GameObject beam1, obstacle1;

    // Phase 2: Obstacles
    public GameObject beam2;

    // Phase 3: Black Hole Demo
    public GameObject beam3;
    public GameObject[] obstacles3;

    // Phase 4: Black Hole
    public GameObject beam4;

    // Use this for initialization
    void Start () {
        currentBeam = beam0;
        isRunning = true;
        RememberInitials();
    }
	
	// Update is called once per frame
	void Update () {
        KeyboardInput();
        Phase0Actions();
        Phase1Actions();
        Phase2Actions();
        Phase3Actions();
        Phase4Actions();
        UpdateGameController();
    }

    void KeyboardInput() {
        if (KeyboardMapping.self.GetKeyDown(KeyAction.P1L)) {
            switch (phase) {
                case 0:
                    p1Control[0] = true;
                    break;
                default:
                    break;
            }
        }
        if (KeyboardMapping.self.GetKeyDown(KeyAction.P1R)) {
            switch (phase) {
                case 0:
                    p1Control[1] = true;
                    break;
                default:
                    break;
            }
        }
        if (KeyboardMapping.self.GetKeyDown(KeyAction.P1CW)) {
            switch (phase) {
                case 0:
                    p1Control[2] = true;
                    break;
                default:
                    break;
            }
        }
        if (KeyboardMapping.self.GetKeyDown(KeyAction.P1CCW)) {
            switch (phase) {
                case 0:
                    p1Control[3] = true;
                    break;
                default:
                    break;
            }
        }
        if (KeyboardMapping.self.GetKeyDown(KeyAction.P2L)) {
            switch (phase) {
                case 0:
                    p2Control[0] = true;
                    break;
                default:
                    break;
            }
        }
        if (KeyboardMapping.self.GetKeyDown(KeyAction.P2R)) {
            switch (phase) {
                case 0:
                    p2Control[1] = true;
                    break;
                default:
                    break;
            }
        }
        if (KeyboardMapping.self.GetKeyDown(KeyAction.P2CW)) {
            switch (phase) {
                case 0:
                    p2Control[2] = true;
                    break;
                default:
                    break;
            }
        }
        if (KeyboardMapping.self.GetKeyDown(KeyAction.P2CCW)) {
            switch (phase) {
                case 0:
                    p2Control[3] = true;
                    break;
                default:
                    break;
            }
        }
        if (KeyboardMapping.self.GetKeyDown(KeyAction.ESC)) {
            skipClick++;
        }

        if (skipClick > 4 && !skipTriggered) {
            skipTriggered = true;
            EndTutorial();
        }
    }

    void UpdateGameController() {    
        distance = currentBeam.transform.position.x - startpoint > distance ? currentBeam.transform.position.x - startpoint : distance;
        GameController.gameController.TutorialSetValues(distance, timer);
    }

    void RememberInitials() {
        startpoint = currentBeam.transform.position.x;
        position1 = player1.transform.position;
        position2 = player2.transform.position;
        rotation1 = player1.transform.rotation;
        rotation2 = player2.transform.rotation;
        velocity1 = player1.GetComponent<Rigidbody>().velocity;
        velocity2 = player2.GetComponent<Rigidbody>().velocity;
    }

    void ResetToInitiails() {
        player1.transform.position = position1;
        player1.transform.rotation = rotation1;
        player2.transform.position = position2;
        player2.transform.rotation = rotation2;
        player1.GetComponent<Rigidbody>().velocity = velocity1;
        player2.GetComponent<Rigidbody>().velocity = velocity2;
        GameController.gameController.ConcludeGame(isTutorial: true);
        distance = 0f;
    }

    void Phase0Actions() {
        if (phase == 0) {
            if (isRunning) timer += Time.deltaTime;
            if (timer > 3.0f) {
                description.SetActive(false);
            }
            if (p1Control[0] && p1Control[1] && p1Control[2] && p1Control[3]) {
                wasdIcon.SetActive(false);
                playerTag1.SetActive(false);
            }
            if (p2Control[0] && p2Control[1] && p2Control[2] && p2Control[3]) {
                arrowIcon.SetActive(false);
                playerTag2.SetActive(false);
            }
            if(!wasdIcon.activeInHierarchy && !arrowIcon.activeInHierarchy) {
                phase ++;
                timer = 0f;
                KeyboardMapping.self.LockKeyboard();
                if (beam0.activeInHierarchy) {
                    beam0.SetActive(false);
                }
            }
        }
    }

    void Phase1Actions() {
        if (phase == 1) {
            timer += Time.deltaTime;

            obstacle1.SetActive(true);
            beam1.SetActive(true);
            currentBeam = beam1;
            GameController.gameController.StartGame();

            demo.SetActive(true);
            description.GetComponent<Text>().text = "Obstacles Will Reflect As Well";
            description.SetActive(true);

            ResetToInitiails();
        }
    }

    void Phase2Actions() {
        if (phase == 2) {
            beam2.SetActive(true);
            currentBeam = beam2;
            GameController.gameController.StartGame();

            timer += Time.deltaTime;
            if (timer > 3.0f) {
                description.SetActive(false);
            }
        }
    }

    void Phase3Actions() {
        if (phase == 3) {
            timer += Time.deltaTime;

            foreach (GameObject obs3 in obstacles3) {
                obs3.SetActive(true);
            }
            beam3.SetActive(true);
            currentBeam = beam3;
            GameController.gameController.StartGame();

            demo.SetActive(true);
            description.GetComponent<Text>().text = "Black Holes Will Kill Transmission";
            description.SetActive(true);

            ResetToInitiails();
        }
    }

    void Phase4Actions() {
        if (phase == 4) {
            beam4.SetActive(true);
            currentBeam = beam4;
            GameController.gameController.StartGame();

            timer += Time.deltaTime;
            if (timer > 3.0f) {
                description.SetActive(false);
            }
        }
    }

    public void SphereDestroyed(string name = "Sphere") {
        Debug.Log(name);
        if (name == "Sphere (4)") {
            EndTutorial();
        }
        if (name == "Sphere (3)") {
            timer = 0f;
            distance = 0f;
            phase++;
            demo.SetActive(false);
            KeyboardMapping.self.UnlockKeyboard();
        }
        if (name == "Sphere (2)") {
            timer = 0f;
            phase++;
            obstacle1.SetActive(false);
            KeyboardMapping.self.LockKeyboard();
        }
        if (name == "Sphere (1)") {
            timer = 0f;
            distance = 0f;
            phase++;
            demo.SetActive(false);
            KeyboardMapping.self.UnlockKeyboard();
        }
        isRunning = false;
    }

    public void EndTutorial() {
        phase = -1;
        GameController.gameController.ConcludeGame(isTutorial: true);
        SceneController.sceneController.ToSceneName("Mirror_LV01 - Local", timer: 0.5f);
    }
}
