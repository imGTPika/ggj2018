﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardMapping : MonoBehaviour {

    public static KeyboardMapping self = null;
    private static BidirectionMapping<KeyCode, KeyAction> keyMap = null;

    private bool locked = false;

    // Use this for initialization
    void Awake () {
        DontDestroyOnLoad(transform.gameObject);
    }

    void Start () {
        if (self == null) {
            self = this;
        }
        else {
            Destroy(this.gameObject);
        }
        keyMap = new BidirectionMapping<KeyCode, KeyAction>();

        // Default Bindings
        keyMap.Add(KeyCode.A, KeyAction.P1L);
        keyMap.Add(KeyCode.D, KeyAction.P1R);
        keyMap.Add(KeyCode.W, KeyAction.P1CW);
        keyMap.Add(KeyCode.S, KeyAction.P1CCW);
        keyMap.Add(KeyCode.LeftArrow, KeyAction.P2L);
        keyMap.Add(KeyCode.RightArrow, KeyAction.P2R);
        keyMap.Add(KeyCode.UpArrow, KeyAction.P2CW);
        keyMap.Add(KeyCode.DownArrow, KeyAction.P2CCW);
        keyMap.Add(KeyCode.Space, KeyAction.SPACE);
        keyMap.Add(KeyCode.Escape, KeyAction.ESC);
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void LockKeyboard() {
        locked = true;
    }

    public void UnlockKeyboard() {
        locked = false;
    }

    public bool GetKey(KeyAction action) {
        if (locked) {
            return false;
        }
        if (Input.GetKey(keyMap.Reverse(action))) {
            return true;
        }
        return false;
    }

    public bool GetKeyDown(KeyAction action) {
        if (locked) {
            return false;
        }
        if (Input.GetKeyDown(keyMap.Reverse(action))) {
            return true;
        }
        return false;
    }

    public bool GetKeyUp(KeyAction action) {
        if (locked) {
            return false;
        }
        if (Input.GetKeyUp(keyMap.Reverse(action))) {
            return true;
        }
        return false;
    }
}

public enum KeyAction {
    P1L,
    P1R,
    P1CW,
    P1CCW,
    P2L,
    P2R,
    P2CW,
    P2CCW,
    SPACE,
    ESC
};

class BidirectionMapping<T1, T2>
{
    private Dictionary<T1, T2> _forward = null;
    private Dictionary<T2, T1> _reverse = null;

    public BidirectionMapping () {
        this._forward = new Dictionary<T1, T2>();
        this._reverse = new Dictionary<T2, T1>();
    }

    public void Add (T1 t1, T2 t2) {
        Remove(t1, t2);
        _forward.Add(t1, t2);
        _reverse.Add(t2, t1);
    }

    public void Remove (T1 t1, T2 t2) {
        if (_forward.ContainsKey(t1))
        {
            _forward.Remove(t1);
        }
        if (_reverse.ContainsKey(t2))
        {
            _reverse.Remove(t2);
        }
    }

    public T2 Forward (T1 t1) {
        return _forward[t1];
    }

    public T1 Reverse (T2 t2) {
        return _reverse[t2];
    }
}
