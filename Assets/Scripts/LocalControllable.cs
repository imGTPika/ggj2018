﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalControllable : MonoBehaviour {
    public PlayerID playerId;
    public float sensitivity;
    public float rotationSensitivity;
    Rigidbody rb;
    private KeyAction leftKey;
    private KeyAction rightKey;
    private KeyAction rotateClockwiseKey;
    private KeyAction rotateCounterClockwiseKey;

    // Use this for initialization
    void Start() {
        rb = GetComponent<Rigidbody>();
        if (playerId == PlayerID.PLAYER1) {
            leftKey = KeyAction.P1L;
            rightKey = KeyAction.P1R;
            rotateClockwiseKey = KeyAction.P1CW;
            rotateCounterClockwiseKey = KeyAction.P1CCW;
        } else {
        leftKey = KeyAction.P2L;
        rightKey = KeyAction.P2R;
        rotateClockwiseKey = KeyAction.P2CW;
        rotateCounterClockwiseKey = KeyAction.P2CCW;
        }
    }

    // Update is called once per frame
    void Update() {

        if (KeyboardMapping.self.GetKey(leftKey)) {
            rb.velocity += new Vector3(-1 * sensitivity, 0, 0);
        }

        if (KeyboardMapping.self.GetKey(rightKey)) {
            rb.velocity += new Vector3(1 * sensitivity, 0, 0);
        }

        if (KeyboardMapping.self.GetKey(rotateClockwiseKey)) {
            rb.angularVelocity += new Vector3(0, 0, -1 * rotationSensitivity);
        }

        if (KeyboardMapping.self.GetKey(rotateCounterClockwiseKey)) {
            rb.angularVelocity += new Vector3(0, 0, 1 * rotationSensitivity);
        }

    }
}
