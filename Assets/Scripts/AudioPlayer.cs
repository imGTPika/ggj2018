﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour {

    public AudioSource source;
    public AudioClip[] clips;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Play(int num) {
        source.clip = clips[num];
        source.Play();
    }

    public void Stop() {
        source.Stop();
    }
}
