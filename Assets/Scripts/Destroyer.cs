﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour {

    public enum Notifier { GAME_CONTROLLER, TUTORIAL, OTHERS};
    public Notifier reactTo;
    public GameObject reactor;

    AudioSource audioSource;
    public AudioClip killedClip;
    private float vol = 1f;

    // Use this for initialization
    void Start () {
        audioSource = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {

    }

    void OnTriggerEnter(Collider col) {
        if (col.gameObject.GetComponent<Bounce>() != null) {
            // GG!
            if (reactTo == Notifier.TUTORIAL) {
                col.gameObject.SetActive(false);
                reactor.GetComponent<TutorialManager>().SphereDestroyed(col.gameObject.name);
            } 
            else {
                if (killedClip != null) {
                    audioSource.PlayOneShot(killedClip, vol);
                }
                col.gameObject.SetActive(false);
                GameController.gameController.ConcludeGame();
            }
        }
    }
}
