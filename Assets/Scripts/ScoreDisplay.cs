﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreDisplay : MonoBehaviour {

    public enum DisplayStatus { MAIN_GAME, TUTORIAL, SCORE_BOARD};

    public DisplayStatus stage;
    public Text distanceText, maxDistanceText;
    public Text surviveTimeText, maxSurviveTimeText;
    public Text velocityText, maxVelocityText;

    private bool refresh = false;

    // Use this for initialization
    void Start () {
        refresh = GameController.gameController.gameFinished;
    }
	
	// Update is called once per frame
	void Update () {
        if (stage == DisplayStatus.MAIN_GAME || stage == DisplayStatus.TUTORIAL) {
            distanceText.text = GameController.gameController.travelDistance.ToString("F2") + " m";
            maxDistanceText.text = GameController.gameController.maxTravelDistance.ToString("F2") + " m";
            surviveTimeText.text = CalculateTime(GameController.gameController.surviveTime);
            velocityText.text = GameController.gameController.velocity.ToString("F2") + " m/s";
        }
        if (stage == DisplayStatus.SCORE_BOARD) {
            distanceText.text = GameController.gameController.travelDistance.ToString("F2") + " m";
            if (refresh && GameController.gameController.travelDistance >= GameController.gameController.maxTravelDistance) {
                maxDistanceText.text = "NEW!!";
            } else { 
                maxDistanceText.text = GameController.gameController.maxTravelDistance.ToString("F2") + " m";
            }

            surviveTimeText.text = CalculateTime(GameController.gameController.surviveTime);
            if (refresh && GameController.gameController.travelDistance >= GameController.gameController.maxTravelDistance) {
                maxSurviveTimeText.text = "NEW!!";
            } else {
                maxSurviveTimeText.text = CalculateTime(GameController.gameController.maxSurviveTime);
            }

            velocityText.text = GameController.gameController.velocity.ToString("F2") + " m/s";
            if (refresh && GameController.gameController.travelDistance >= GameController.gameController.maxTravelDistance) {
                maxVelocityText.text = "NEW!!";
            } else {
                maxVelocityText.text = GameController.gameController.maxVelocity.ToString("F2") + " m/s";
            }
        }
    }

    private string CalculateTime(float timef) {
        string minSec = "";
        int time = (int)timef;
        if (time >= 60) {
            minSec += (time / 60).ToString() + " m ";
        }
        minSec += (time % 60).ToString() + " s";
        return minSec;
    }
}
