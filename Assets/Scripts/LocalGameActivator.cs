﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalGameActivator : MonoBehaviour {

    public GameObject beam;
    public CameraMovement cam;

    private bool triggerSceneChange = false;
    private float stallTimer = 3f;

	// Use this for initialization
	void Start () {
        GameController.gameController.StartGame(beam);
	}
	
	// Update is called once per frame
	void Update () {
        if (GameController.gameController.gameFinished) {
            cam.speed = 0f;
            if (stallTimer > 0f) {
                stallTimer -= Time.deltaTime;
            }
            if (stallTimer < 0f && !triggerSceneChange) {
                triggerSceneChange = true;
                SceneController.sceneController.ToSceneName("Credit");
            }
        }
	}


}
