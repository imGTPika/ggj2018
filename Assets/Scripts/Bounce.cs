﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bounce : MonoBehaviour {
    public Vector3 initialVelocity; //velocity
    Vector3 v;
    Rigidbody rb;
    AudioSource audioSource;
    public AudioClip[] collidePlayer1Clips;
    public AudioClip collidePlayer2Clip;
    public AudioClip[] collideObstacleClips;
    public float vol;

    private int p1_clip = 0;

    // Use this for initialization
    void Start () {
        v = initialVelocity;
        rb = GetComponent<Rigidbody>();
        rb.velocity = v;
    }

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }


    // Update is called once per frame
    void Update () {
        //Vector3 newPosition = transform.position + v;
        //transform.position.Set(newPosition.x, newPosition.y, newPosition.z);
        rb.velocity = v;

    }

    void OnCollisionEnter(Collision col)
    {
        Reflective reflectiveObject = col.gameObject.GetComponentInChildren<Reflective>();
        if (reflectiveObject != null)
        {  //need to check if it is reflective object
            // Debug.Log("mirror object collided!");
            Vector3 reflectionDelta = Vector3.Dot(v, reflectiveObject.getNormal()) * reflectiveObject.getNormal();
            v -= 2 * reflectionDelta;

            if (col.gameObject.GetComponent<Controllable>() != null || col.gameObject.GetComponent<LocalControllable>() != null) {
                if (col.gameObject.name == "m1") {
                    audioSource.PlayOneShot(collidePlayer1Clips[p1_clip], vol);
                    p1_clip = (p1_clip + 1) % collidePlayer1Clips.Length;
                }
                else if (col.gameObject.name == "m2") {
                    audioSource.PlayOneShot(collidePlayer2Clip, vol);
                } 
                else {

                }
            }
            else {
                int k = Random.Range(0, collideObstacleClips.Length);
                audioSource.PlayOneShot(collideObstacleClips[k], vol);
            }
        }
 
    }
}
