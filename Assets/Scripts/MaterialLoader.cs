﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialLoader : MonoBehaviour {
    //public Renderer background;
    public Renderer[] spheres;
    public Renderer player1;
    public Renderer player2;
    public Renderer[] obstacles1;
    public Renderer[] obstacles2;
    public Renderer[] obstacles3;
    //public Material m_background;
    public Material m_sphere;
    public Material m_player1;
    public Material m_player2;
    public Material m_obstacle1;
    public Material m_obstacle2;
    public Material m_obstacle3;


    // Use this for initialization
    void Start ()
    {
       // background.material = m_background;
        foreach (Renderer sphere in spheres) {
            sphere.material = m_sphere;
        }
        player1.material = m_player1;
        player2.material = m_player2;
        foreach (Renderer obstacle in obstacles1)
        {
            obstacle.material = m_obstacle1;
        }

        foreach (Renderer obstacle in obstacles2)
        {
            obstacle.material = m_obstacle2;
        }

        foreach (Renderer obstacle in obstacles3)
        {
            obstacle.material = m_obstacle3;
        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
