﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    public static GameController gameController = null;

    // Best Records
    public int maxBounceTimes { get; private set; }
    public float maxTravelDistance {get; private set; }
    public float maxSurviveTime {get; private set; }
    public float maxVelocity {get; private set; }

    // Current game record
    public int bounceTimes { get; private set; }
    public float travelDistance { get; private set; }
    public float surviveTime { get; private set; }
    public float velocity { get; private set; }

    // Current game flags
    private bool timerOn;
    private float startX;
    private GameObject currentBeam;
    public bool gameFinished = false;

    // Use this for initialization
    void Start () {
        if (gameController == null) {
            gameController = this;
            // Init History Best
            maxBounceTimes = 0;
            maxTravelDistance = maxSurviveTime = maxVelocity = 0f;
            // Init Local
            Initiate();
        }
        else {
            Destroy(this.gameObject);
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (timerOn) {
            travelDistance = currentBeam.transform.position.x - startX > travelDistance ? currentBeam.transform.position.x - startX : travelDistance;
            surviveTime += Time.deltaTime;
            velocity = surviveTime == 0f ? surviveTime : travelDistance / surviveTime;
        }
    }

    public void TutorialSetValues(float distance, float time) {
        travelDistance = distance;
        surviveTime = time;
        velocity = time == 0f ? time : distance / time;
    }

    public void Initiate() {
        timerOn = false;
        gameFinished = false;
        bounceTimes = 0;
        travelDistance = surviveTime = velocity = 0f;
    }

    /// <summary>
    /// Start Game
    /// </summary>
    public void StartGame(GameObject beam = null) {
        Initiate();
        if (beam != null) {
            startX = beam.transform.position.x;
            currentBeam = beam;
        } else {
            return;
        }
        timerOn = true;
    }

    public void ConcludeGame(bool isTutorial = false) {
        timerOn = false;

        // Check if overwrite best records
        if(!isTutorial && travelDistance > maxTravelDistance) {
            maxBounceTimes = bounceTimes;
            maxTravelDistance = travelDistance;
            maxSurviveTime = surviveTime;
            maxVelocity = velocity;
        }
        gameFinished = true;
    }
}
