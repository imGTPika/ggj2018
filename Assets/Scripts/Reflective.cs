﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reflective : MonoBehaviour
{
    public Vector3 initalNormal;
    Vector3 normal;
    // Use this for initialization
    void Start()
    {
        normal = transform.rotation* initalNormal;
    }

    // Update is called once per frame
    void Update()
    {
        normal = transform.rotation * initalNormal;
        //Debug.DrawLine(transform.position, transform.position + normal, Color.red);
    }

    public Vector3 getNormal() { return normal; }
}
