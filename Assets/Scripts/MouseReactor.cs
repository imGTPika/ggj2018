﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseReactor : MonoBehaviour {

    public enum ActionList {
        TO_NEXT_SCENE,
        TO_TITLE_SCENE,
        TO_SCENE_ID,
        TO_SCENE_NAME,
        QUIT_GAME,
        NO_ACTION,
        SPECIAL_USE_CASE
    };

    public ActionList actionItem;
    public int sceneId;
    public string sceneName;
    
    // Use this for initialization
    void Start() {
    }

    // Update is called once per frame
    void Update() {
        if (actionItem == ActionList.SPECIAL_USE_CASE) {
            GameController.gameController.gameFinished = false;
            GameController.gameController.Initiate();
        }
    }

    /// <summary>
    /// Action after mouse click
    /// </summary>
    private void OnMouseDown() {
        switch (actionItem) {
            case ActionList.TO_NEXT_SCENE:
                SceneController.sceneController.NextScene();
                break;
            case ActionList.TO_TITLE_SCENE:
                SceneController.sceneController.ToSceneName("Title");
                break;
            case ActionList.TO_SCENE_NAME:
                SceneController.sceneController.ToSceneName(sceneName);
                break;
            case ActionList.TO_SCENE_ID:
                SceneController.sceneController.ToSceneId(sceneId);
                break;
            case ActionList.QUIT_GAME:
                Application.Quit();
                break;
            default:
                break;
        }
    }
}