﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCameraControl : MonoBehaviour {

    public GameObject cam;
    public float cameraVelocity = 1f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        MoveCamera();
    }

    void MoveCamera() {
        Vector3 start = transform.position;
        Vector3 end = start + new Vector3(cameraVelocity, 0f, 0f);
        cam.transform.position = Vector3.Lerp(start, end, Time.deltaTime);
    }
}
