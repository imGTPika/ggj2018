﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {

    public static SceneController sceneController = null;
    public SpriteRenderer fader;
    public Text[] UIs;

    private const short STATE_NULL = 0, STATE_SCENE_ID = 1, STATE_SCENE_NAME = 2;
    private short state;
    private int nextSceneID;
    private string nextSceneName;
    private float realTimer, fadeTime, stallTime;

    // Use this for initialization
    void Start () {
		sceneController = this;
        Initialize();
    }

    // Update is called once per frame
    void Update() {
        if (realTimer > 0f) {
            realTimer -= Time.deltaTime;
            _Fade();
        }
        if (realTimer <= 0f && state != STATE_NULL) {
            _ChangeScene();
        }
    }

    /// <summary>
    /// Init values
    /// </summary>
    private void Initialize() {
        state = STATE_NULL;
        nextSceneName = null;
        nextSceneID = 0;
        fadeTime = 0f;
        stallTime = 1f;
    }

    /// <summary>
    /// Fading
    /// </summary>
    private void _Fade() {
        Color curColor = fader.color;
        curColor.a = Mathf.Lerp(1f, 0f, (realTimer - stallTime) / (fadeTime - stallTime));
        fader.color = curColor;

        foreach (Text ui in UIs) {
            Color uicolor = ui.color;
            uicolor.a = Mathf.Lerp(0f, 1f, (realTimer - stallTime) / (fadeTime - stallTime));
            ui.color = curColor;
        }
    }

    /// <summary>
    /// Actually doing the scene change
    /// </summary>
    private void _ChangeScene() {
        switch (state) {
            case STATE_SCENE_ID:
                SceneManager.LoadScene(nextSceneID);
                break;
            case STATE_SCENE_NAME:
                SceneManager.LoadScene(nextSceneName);
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Call next scene
    /// </summary>
    /// <param name="timer">time to fade out</param>
    public void NextScene (float timer = 1f) {
        fadeTime = realTimer = timer + stallTime;
        state = STATE_SCENE_ID;
        nextSceneID = SceneManager.GetActiveScene().buildIndex + 1;
    }

    /// <summary>
    /// Call a specific scene with a given scene id
    /// </summary>
    /// <param name="sceneId">ID of the scene</param>
    /// <param name="timer">time to fade out</param>
    public void ToSceneId(int sceneId, float timer = 1f) {
        fadeTime = realTimer = timer + stallTime;
        state = STATE_SCENE_ID;
        nextSceneID = sceneId;
    }

    /// <summary>
    /// Call a specific scene with a given scene name
    /// </summary>
    /// <param name="sceneName">name of the scene</param>
    /// <param name="timer">time to fade out</param>
    public void ToSceneName (string sceneName, float timer = 1f) {
        fadeTime = realTimer = timer + stallTime;
        state = STATE_SCENE_NAME;
        nextSceneName = sceneName;
    }

}
