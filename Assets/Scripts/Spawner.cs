﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject[] spawnObjects;
    private GameObject currentSpawn = null;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void Spawn () {
        if (currentSpawn == null) {
            currentSpawn = Instantiate(spawnObjects[0], transform.position, Quaternion.identity);
            currentSpawn.transform.SetParent(gameObject.transform);
        }
    }
}
