﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class GameActivator : NetworkBehaviour
{
    public NetworkManager nm;
    public GameObject cam;
    public GameObject spherePrefab;

    GameObject objectToDestroy;
    RestartGame[] objectsToReset;
    GameObject cameraObject;

    private bool isMovingScene;
    private bool clientGameStart;
    public float stallTimer = 0f;
    
    [SyncVar]
    public bool sphereActivation;

    // Use this for initialization
    void Start()
    {
        ClientScene.RegisterPrefab(spherePrefab);
        sphereActivation = false;

        clientGameStart = false;
        isMovingScene = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ResetGame();
            CmdResetGame();
        }

        if (!isServer)
        {
            cam.GetComponent<CameraMovement>().speed = 0.025f;
            if (clientGameStart) {
                clientGameStart = true;
                GameController.gameController.StartGame(GameObject.FindObjectOfType<Bounce>().gameObject);
            }
            return;
        }

        if (GameController.gameController.gameFinished) {
            stallTimer += Time.deltaTime;
            if (stallTimer > 5f && !isMovingScene) {
                isMovingScene = true;
                SceneController.sceneController.ToSceneName("Credit");
            }
        }

        // got two players, start game!
        if (nm.numPlayers == 2)
        {
            if (sphereActivation == false)
            {
                InstantiateSphere();
                cam.GetComponent<CameraMovement>().speed = 0.025f;

                sphereActivation = true;
            }
        }
    }

    void ResetGame()
    {
        stallTimer = 0f;

        objectsToReset = GameObject.FindObjectsOfType<RestartGame>();
        foreach (RestartGame objectToReset in objectsToReset)
        {
            objectToReset.ResetGame();
        }

        if (isServer) {
            objectToDestroy = GameObject.FindWithTag("DestroyOnReset");
            NetworkServer.Destroy(objectToDestroy);
        }

    }

    [Command]
    void CmdResetGame()
    {
        objectToDestroy = GameObject.FindWithTag("DestroyOnReset");
        NetworkServer.Destroy(objectToDestroy);
        InstantiateSphere();
    }

    void InstantiateSphere() {
        GameObject sphere = Instantiate(spherePrefab, spherePrefab.transform.position, spherePrefab.transform.rotation);
        GameController.gameController.StartGame(sphere);
        NetworkServer.Spawn(sphere);
    }
}
