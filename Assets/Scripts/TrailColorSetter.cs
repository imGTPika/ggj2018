﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailColorSetter : MonoBehaviour {
    TrailRenderer trail;
    MeshRenderer sphere;
    // Use this for initialization
    void Start () {
        trail = GetComponent<TrailRenderer>();
        sphere = GetComponent<MeshRenderer>();
        trail.startColor = sphere.material.color;
    }
	
	// Update is called once per frame
	void Update () {
        trail.startColor = sphere.material.color;
    }
}
